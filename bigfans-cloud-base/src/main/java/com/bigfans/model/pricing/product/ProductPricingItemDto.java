package com.bigfans.model.pricing.product;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductPricingItemDto {

    private BigDecimal originalPrice;
    private BigDecimal finalPrice;
    private String activityInfo;
    private String activityId;

}
