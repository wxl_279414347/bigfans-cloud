package com.bigfans.orderservice.ordertracker;

import com.bigfans.framework.utils.DateUtils;
import lombok.Data;

import java.util.Date;

@Data
public class ExpiredOrderTrack {

    private String orderId;
    private Date expiredTime;

    public ExpiredOrderTrack() {
    }

    public ExpiredOrderTrack(String orderId, Date expiredTime) {
        this.orderId = orderId;
        this.expiredTime = expiredTime;
    }

    public boolean hasExpired(){
        return DateUtils.compare(new Date() , expiredTime) > 0;
    }

}
