package com.bigfans.orderservice.listeners;

import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.framework.utils.JsonUtils;
import com.bigfans.model.event.ProductCreatedEvent;
import com.bigfans.orderservice.api.clients.CatalogServiceClient;
import com.bigfans.orderservice.model.Product;
import com.bigfans.orderservice.model.ProductSpec;
import com.bigfans.orderservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-03-16 下午7:59
 **/
@KafkaConsumerBean
@Component
public class ProductListener {

    @Autowired
    private ProductService productService;
    @Autowired
    private CatalogServiceClient catalogServiceClient;

    @KafkaListener
    public void on(ProductCreatedEvent event) {
        try {
            String prodId = event.getId();
            CompletableFuture<Product> prodFuture = catalogServiceClient.getProductById(prodId);
            Product product = prodFuture.get();
            productService.create(product);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
