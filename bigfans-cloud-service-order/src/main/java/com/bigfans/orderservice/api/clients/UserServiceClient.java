package com.bigfans.orderservice.api.clients;

import com.bigfans.api.clients.ServiceRequest;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.orderservice.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-13 下午7:42
 **/
@Component
public class UserServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Void> useProperty(CurrentUser currentUser, String orderId, String couponId, Float points, BigDecimal balance) {
        return CompletableFuture.supplyAsync(() -> {
            Map<String, Object> payload = new HashMap<>();
            payload.put("id", orderId);
            payload.put("couponId", couponId);
            payload.put("points", points);
            payload.put("balance", balance);
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, currentUser);
            serviceRequest.post("http://user-service/useProperty", "", payload);
            return null;
        });
    }

    public CompletableFuture<Address> myAddress(CurrentUser currentUser, String addressId) {
        return CompletableFuture.supplyAsync(() -> {
            ServiceRequest serviceRequest = new ServiceRequest(restTemplate, currentUser);
            Map data = serviceRequest.get(Map.class, "http://user-service/myAddress/{addressId}", addressId);
            Address address = BeanUtils.mapToModel(data, Address.class);
            return address;
        });
    }
}
