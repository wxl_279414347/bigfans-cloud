package com.bigfans.orderservice.api.form;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lichong
 * @create 2018-03-15 下午7:52
 **/
@Data
public class OrderCreateForm {

    // 支付类型ID
    private String payMethodCode;
    // 地址
    private String addressId;
    private BigDecimal prodTotalPrice;
    // 总价格(计算完邮费和各种优惠后的应付款额)
    private BigDecimal totalPrice;
    private Float usedPoints;
    private String usedCouponId;
    private BigDecimal usedBalance;
    private String invoiceType;
    private String invoiceTitle;
    private String invoiceTitleType;
    private String invoiceContentType;
    private String invoicePhone;
    private String invoiceEmail;

    private List<OrderCreateItemForm> items;
}
