package com.bigfans.searchservice.model.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @create 2018-03-18 下午2:19
 **/
@Data
public class SearchFilter {

    private String optionId;
    private String optionName;
    private List<SearchFilterItem> values;

    public void addValue(SearchFilterItem item){
        if(values == null){
            values = new ArrayList<>();
        }
        values.add(item);
    }
}
