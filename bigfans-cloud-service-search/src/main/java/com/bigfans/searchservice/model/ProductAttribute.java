package com.bigfans.searchservice.model;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-02-25 下午2:00
 **/
@Data
public class ProductAttribute {

    private String prodId;
    private String optionId;
    private String optionName;
    private String valueId;
    private String value;
    private String inputType;

}
