package com.bigfans.cartservice.model;

import com.bigfans.cartservice.model.entity.ProductEntity;
import lombok.Data;

import java.util.List;

/**
 * 
 * @Description:商品
 * @author lichong 2015年3月18日上午10:48:41
 *
 */
@Data
public class Product extends ProductEntity {

	private static final long serialVersionUID = -4961274117193266934L;

	// 表单项: 规格列表
	private List<ProductSpec> specList;
	
}
