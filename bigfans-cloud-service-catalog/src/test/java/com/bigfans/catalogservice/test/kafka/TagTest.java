package com.bigfans.catalogservice.test.kafka;

import com.bigfans.catalogservice.CatalogServiceApp;
import com.bigfans.catalogservice.model.Tag;
import com.bigfans.catalogservice.service.tag.TagService;
import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.kafka.KafkaTemplate;
import com.bigfans.model.event.CategoryCreatedEvent;
import com.bigfans.model.event.tag.TagCreatedEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author lichong
 * @create 2018-03-18 下午3:17
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CatalogServiceApp.class)
public class TagTest {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private TagService tagService;

    @Test
    public void test() throws Exception {
        List<Tag> tags = tagService.listByProdId("374");
        for (Tag tag: tags) {
            TagCreatedEvent event = new TagCreatedEvent(tag.getId());
            event.setName(tag.getName());
            event.setRelatedCount(tag.getRelatedCount());
            kafkaTemplate.send(event);
        }
    }

    @Test
    public void testSend(){
        TagCreatedEvent t1 = new TagCreatedEvent();
        t1.setName("口香糖");
        t1.setRelatedCount(32);
        kafkaTemplate.send(t1);

        TagCreatedEvent t2 = new TagCreatedEvent();
        t2.setName("益达");
        t2.setRelatedCount(13);
        kafkaTemplate.send(t2);
    }


}
