package com.bigfans.framework.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import com.bigfans.framework.web.session.RedisSessionProxyFactory;


/**
 * 
 * @Title: 
 * @Description: request的代理对象
 * @author lichong 
 * @date 2016年1月25日 上午11:45:22 
 * @version V1.0
 */
public class WrappedHttpServletRequest extends HttpServletRequestWrapper {

	public WrappedHttpServletRequest(HttpServletRequest request) {
		super(request);
	}

	@Override
	public HttpSession getSession() {
		return RedisSessionProxyFactory.wrap(super.getSession());
	}
	
	@Override
	public HttpSession getSession(boolean create) {
		return RedisSessionProxyFactory.wrap(super.getSession(create));
	}

}
